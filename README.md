# Overview

This repository contains supplementary materials for the following conference paper:

Jan Vykopal, Pavel Čeleda, Valdemar Švábenský, Martin Hofbauer, and Martin Horák.\
**Research and Practice of Delivering Tabletop Exercises**\
In Proceedings of the 29th Conference on Innovation and Technology in Computer Science Education (ITiCSE 2024).\
https://doi.org/10.1145/3649217.3653642

Preprint: https://arxiv.org/abs/2404.10206

The materials are freely available for further use under the [CC BY 4.0 license](https://creativecommons.org/licenses/by/4.0/).

# Contents of the repository

* `papers.csv` - details of 14 research papers selected for the review, structured according to the research questions
* `projects.csv` - details of 16 practical GitHub projects related to the preparation and delivery of tabletop exercises

# How to cite

If you use or build upon the materials, please use the BibTeX entry below to cite the original paper (not only this web link).

```bibtex
@inproceedings{Vykopal2024research,
    author    = {Vykopal, Jan and \v{C}eleda, Pavel and \v{S}v\'{a}bensk\'{y}, Valdemar and Hofbauer, Martin and Hor\'{a}k, Martin},
    title     = {{Research and Practice of Delivering Tabletop Exercises}},
    booktitle = {Proceedings of the 29th Conference on Innovation and Technology in Computer Science Education},
    series    = {ITiCSE '24},
    publisher = {Association for Computing Machinery},
    address   = {New York, NY, USA},
    year      = {2024},
    pages     = {220--226},
    numpages  = {7},
    isbn      = {979-8-4007-0600-4},
    url       = {https://doi.org/10.1145/3649217.3653642},
    doi       = {10.1145/3649217.3653642},
}
```